# Banco de Ideias
Coleção pública de ideias de projetos que podem ser implementadas por qualquer pessoa do mundo (ou de fora do mundo) que se interesse. Pode também servir para fomentar parcerias e/ou relações de tutor/pupilo para a troca de conhecimentos.

# Como adicionar novas idéias
Crie um arquivo de texto com o nome `ideia-<id-numerico>.txt` e descreva à vontade a ideia de seu projeto.

# Onde saber mais
A ideia número zero contém maiores detalhes sobre esse Banco de Ideias. Leia o arquivo `ideia-0000.txt`.

# Status legal do conteúdo desse repositório
Tudo o que está nesse repositório é considerado lançado ao domínio público e isso é feito por meio do uso da declaração CC0 do projecto Creative Commons. Veja o texto completo no arquivo `CC0.txt`.
